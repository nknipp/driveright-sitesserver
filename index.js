'use strict';

const fs = require('fs');
const path = require('path');
const http = require('http');
const mime = require('mime');
const url = require('url');
const util = require('util');

const existsAsync = util.promisify(fs.stat);
const readFileAsync = util.promisify(fs.readFile);

const port = process.env.PORT;
const sitesBase = process.env.SITESBASE;

const server = http.createServer(async (req, res) => {
  const host = req.headers['host'];
  const hostname = host.split(':', 1)[0];

  const startsWithWww = hostname.startsWith('www.');
  const isSecured = req.headers['x-forwarded-proto'] === 'https';

  if (!startsWithWww || !isSecured) {
    res.writeHead(301, {
      'Location': `https://${!startsWithWww ? 'www.' : ''}${host}${req.url}`
    });
    return res.end();
  }

  const reqUrl = url.parse(req.url);
  //TODO ../ gegen nichts ersetzen
  //TODO Aus dem Cache holen: key = (hostname + url)
  //TODO Ausliefern: buf + mimeType

  try {
    let sitePath = path.join(sitesBase, hostname, 'public', reqUrl.pathname);
    if (sitePath.endsWith('/')) {
      sitePath += 'index.html';
    } else {
      const stat = await existsAsync(sitePath);
      if (stat.isDirectory()) sitePath += '/index.html';
    }
    const buf = await readFileAsync(sitePath);
    //TODO In Cache ablegen: key = (hostname + url), value = buf
    res.statusCode = 200;
    res.setHeader('Content-Type', mime.getType(sitePath));
    res.end(buf);
  } catch (err) {
    console.log(err);
    res.statusCode = 404;
    res.end();
  }
});

server.listen(port, () => {
  console.log(`Server running at ${port}`);
});